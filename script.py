import sys
import pandas as pd
import os


class Dataset:
    """
    Выводит статистику по данным из файла.
    Обрабатывает csv файлы с разделителем в виде двоеточия(;).
    """

    SEPARATOR = ';'

    NAME_COLUMN_PROFIT = 'Profit'
    NAME_COLUMN_SALES = 'Sales'
    NAME_COLUMN_QUANTITY = 'Quantity'

    NAME_COLUMN_PRODUCT = 'Product Name'

    NAME_COLUMN_ORDER_DAY = 'Order Date'
    NAME_COLUMN_SHIP_DAY = 'Ship Date'

    COLUMNS_EVALUATING_PRODUCTS = {
        NAME_COLUMN_SALES: 'по продажам',
        NAME_COLUMN_QUANTITY: 'по количеству продаж',
        NAME_COLUMN_PROFIT: 'по профиту',
    }

    _grouped_data_frame = None

    def __init__(self, path_to_file: str):
        if not self._validate_file(path_to_file):
            raise ValueError('Файл не прошел валидацию')

        self.data_frame = pd.read_csv(path_to_file, header=0,
                                      sep=self.SEPARATOR)
        self.check_all_column()

        self.data_frame[self.NAME_COLUMN_PROFIT] = self.data_frame[
            self.NAME_COLUMN_PROFIT
        ].str.replace(',', '.')
        self._cast_to_concrete_type(self.NAME_COLUMN_PROFIT, float)

        self.data_frame[self.NAME_COLUMN_SALES] = self.data_frame[
            self.NAME_COLUMN_SALES
        ].str.replace(',', '.')
        self._cast_to_concrete_type(self.NAME_COLUMN_SALES, float)

    @staticmethod
    def _validate_file(path_to_file: str) -> bool:
        if not os.path.exists(path_to_file) or not os.path.isfile(path_to_file):
            raise ValueError(
                'Не удалось обнаружить файл по указаному пути. '
                f'Проверьте правильность пути к файлу: {path_to_file}'
            )

        filename, file_extension = os.path.splitext(path_to_file)
        if file_extension != '.csv':
            raise ValueError(
                'Неверный формат файла. Ожидается расширение csv'
            )

        return True

    def get_common_profit(self) -> float:
        """ Общий профит. """
        return sum(
            float(elem)
            for elem in self.data_frame[self.NAME_COLUMN_PROFIT]
        )

    def show_best_product(self):
        """
        Лучшие по продажам, по количеству продаж и по профиту.
        :return:
        """
        data_frame = self._get_grouped_dataframe()

        for name_column, message in self.COLUMNS_EVALUATING_PRODUCTS.items():
            best_sales = data_frame[
                data_frame[name_column] == data_frame[name_column].max()
                ]

            print(
                f'Лучшие продукты {message}: ' +
                ' , '.join((name for name, _ in best_sales.iterrows()))
            )

    def show_worst_product(self):
        """
        Худшие по продажам, по количеству продаж и по профиту.
        :return:
        """
        data_frame = self._get_grouped_dataframe()

        for name_column, message in self.COLUMNS_EVALUATING_PRODUCTS.items():
            worst_sales = data_frame[
                data_frame[name_column] == data_frame[name_column].min()
                ]

            print(
                f'Худшие продукты {message}: ' +
                ' , '.join((name for name, _ in worst_sales.iterrows()))
            )

    def show_average_data_about_delivery(self):
        """ Cредний срок доставки товара клиенту и стандартное отклонение. """
        self.data_frame['Ship Date'] = self.data_frame['Ship Date'].astype(
            'datetime64[ns]'
        )
        self.data_frame['Order Date'] = self.data_frame['Order Date'].astype(
            'datetime64[ns]'
        )
        diff_time = self.data_frame['Ship Date'] - self.data_frame['Order Date']

        print(
            'Cредний срок доставки товара клиенту: ' + str(diff_time.mean())
        )
        print(
            'Стандартное отклонение от среднего срока доставки товара клиенту: '
            + str(diff_time.std())
        )

    def check_all_column(self):
        self._check_column_in_df(name for name in (
            self.NAME_COLUMN_PROFIT,
            self.NAME_COLUMN_SALES,
            self.NAME_COLUMN_QUANTITY,
            self.NAME_COLUMN_PRODUCT,
            self.NAME_COLUMN_ORDER_DAY,
            self.NAME_COLUMN_SHIP_DAY
        ))

    def write_common_data_to_csv(self):
        """
        Запись в файл продаж, их количество и профит по каждому продукту.
        :return:
        """
        data_frame = self._get_grouped_dataframe()
        data_frame.to_csv(
            'SalesOrders.csv',
            columns=[
                self.NAME_COLUMN_SALES,
                self.NAME_COLUMN_QUANTITY,
                self.NAME_COLUMN_PROFIT
            ],
            sep=self.SEPARATOR,
            header=True,
            index=True
        )

    def _check_column_in_df(self, name_column):
        if name_column not in self.data_frame:
            ValueError(f'Не найден столбец {name_column}')

    def _cast_to_concrete_type(self, name_column, data_type):
        self.data_frame[name_column] = self.data_frame[name_column].apply(
            data_type
        )

    def _get_grouped_dataframe(self):
        if self._grouped_data_frame is None:
            self._grouped_data_frame = self.data_frame.groupby(
                self.NAME_COLUMN_PRODUCT
            ).aggregate(sum)

        return self._grouped_data_frame


if __name__ == '__main__':
    try:
        sys_args = sys.argv
        len_sys_args = len(sys_args)
        if len_sys_args < 2:
            raise ValueError(
                'Необходимо добавить в аргументы путь до csv файла'
            )

        if len_sys_args > 2:
            raise ValueError(
                'Передайте в интерпретатор только два аргумента: '
                'скрипт запуска и путь до csv-файла с данными'
            )

        _, path_to_file = sys.argv
        dataset = Dataset(path_to_file)

        print('Общий профит: ' + str(dataset.get_common_profit()))

        dataset.show_best_product()
        dataset.show_worst_product()

        dataset.show_average_data_about_delivery()

        dataset.write_common_data_to_csv()

    except Exception as error:
        print('Ошибка: ' + str(error))
